<?php

/*
 * GD Watermark Lib Plugin
 *
 * This plugin allows you to create watermark on your photos
 *
 * @package PhpThumb
 * @subpackage Plugins
 */

class GdWatermarkExtLib {
	/*
	 * Instance of GdThumb passed to this class
	 * @var GdThumb
	 */

	protected $parentInstance;

	/*
	 * Create a watermark on working image, where $image_path is the watermark image file
	 * @param string $image_path
	 * @param int $right_margin
	 * @param int $bottom_margin
	 * @return object
	 */

	public function watermarkExt($watermark, $that) {

		$this->parentInstance = $that;
		$workingImage = $this->parentInstance->getWorkingImage();

		if (is_string($watermark)) {
			try {
				$watermark = PhpThumbFactory::create($watermark);
			} catch (Exception $e) {
				return $that;
			}
		}

		$imgSize = $this->parentInstance->getCurrentDimensions();
		$watermarkSize = $watermark->getCurrentDimensions();

		$padding = 200;

		if ($watermarkSize['width'] >= $imgSize['width'] || $watermarkSize['height'] >= $imgSize['height']) {

			$watermark->resize($imgSize['width'] * 0.9, $imgSize['height'] * 0.9);
			$watermarkSize = $watermark->getCurrentDimensions();
		}

		$wCount = floor($imgSize['width'] / ($watermarkSize['width'] + $padding));
		$hCount = floor($imgSize['height'] / ($watermarkSize['height'] + $padding));

		if ($wCount < 1)
			$wCount = 1;

		if ($hCount < 1)
			$hCount = 1;

		if ($wCount * $hCount == 1)
			$padding = 0;

		$startW = $imgSize['width'] / 2 - ($wCount * ($watermarkSize['width'] + $padding)) / 2;
		$startH = $imgSize['height'] / 2 - ($hCount * ($watermarkSize['height'] + $padding)) / 2;

		$watermarkWorkingImage = $watermark->getOldImage();

		imageAlphaBlending($workingImage, 1);
		imageAlphaBlending($watermarkWorkingImage, 1);

		imagesavealpha($workingImage, 1);
		imagesavealpha($watermarkWorkingImage, 1);

		for ($w = 0; $w < $wCount; $w++)
			for ($h = 0; $h < $hCount; $h++)
				imagecopy($workingImage, $watermarkWorkingImage, $startW + $padding / 2 + $w * ($watermarkSize['width'] + $padding), $startH + $padding / 2 + $h * ($watermarkSize['height'] + $padding), 0, 0, $watermarkSize['width'], $watermarkSize['height']);

		$this->parentInstance->setOldImage($workingImage);

		return $that;
	}

}

$pt = PhpThumb::getInstance();
$pt->registerPlugin('GdWatermarkExtLib', 'gd');
