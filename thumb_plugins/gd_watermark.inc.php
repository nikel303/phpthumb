<?php

/*
 * GD Watermark Lib Plugin Definition File
 *
 * This file contains the plugin definition for the GD Watermark Lib for PHP Thumb
 *
 * PHP Version 5 with GD 2.0+
 * PhpThumb : PHP Thumb Library <http://phpthumb.gxdlabs.com>
 * Copyright (c) 2009, Ian Selby/Gen X Design
 *
 * Author(s): Cleber Willian dos Santos <e@binho.net>
 *
 * Licensed under the MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author Cleber Willian dos Santos <e@binho.net>
 * @copyright Copyright (c) 2009 Gen X Design
 * @link http://phpthumb.gxdlabs.com
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version 3.0
 * @package PhpThumb
 * @filesource
 */

/*
 * GD Watermark Lib Plugin
 *
 * This plugin allows you to create watermark on your photos
 *
 * @package PhpThumb
 * @subpackage Plugins
 */

class GdWatermarkLib {
    /*
     * Instance of GdThumb passed to this class
     * @var GdThumb
     */

    protected $parentInstance;

    /*
     * Create a watermark on working image, where $image_path is the watermark image file
     * @param string $image_path
     * @param int $right_margin
     * @param int $bottom_margin
     * @return object
     */

    public function watermark($watermark, $that) {

        $this->parentInstance = $that;
        $workingImage = $this->parentInstance->getWorkingImage();

        try {
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        if (is_string($watermark)) {
            try {
                $watermark = PhpThumbFactory::create($watermark);
            } catch (Exception $e) {
                return $that;
            }
        }

        $imgSize = $this->parentInstance->getCurrentDimensions();
        $watermarkSize = $watermark->getCurrentDimensions();

        if ($watermarkSize['width'] > $imgSize['width'] || $watermarkSize['height'] > $imgSize['height']) {
            
            $watermark->resize($imgSize['width'], $imgSize['height']);
            $watermarkSize = $watermark->getCurrentDimensions();
        }
        
        $watermarkWorkingImage = $watermark->getOldImage();

        imageAlphaBlending($workingImage, 1);
        imageAlphaBlending($watermarkWorkingImage, 1);
        imagesavealpha($workingImage, 1);
        imagesavealpha($watermarkWorkingImage, 1);

        imagecopy($workingImage, $watermarkWorkingImage, $imgSize['width'] / 2 - $watermarkSize['width'] / 2, $imgSize['height'] / 2 - $watermarkSize['height'] / 2, 0, 0, $watermarkSize['width'], $watermarkSize['height']);

        $this->parentInstance->setOldImage($workingImage);

        return $that;
    }

}

$pt = PhpThumb::getInstance();
$pt->registerPlugin('GdWatermarkLib', 'gd');